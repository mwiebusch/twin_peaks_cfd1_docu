

# Twin_Peaks_CFD1 - overview


![](TPCFD1_board_photo.jpg)

Twin_Peaks_CFD1 (aka TwinPeaks v2) is an analog add-on board for the TAMEX4 TDC card - both were developed by the experiment electronics department at GSI, Darmstadt.
Twin_Peaks_CFD1 + TAMEX4 are to be operated as a part of an MBS data acquisition system. Alternatively Twin_Peaks_CFD1 can be used with a TDC implemented on a TRB5sc board (not actively supported by our group).
Twin_Peaks_CFD1 inherited its name from its predecessor "TWIN_PEAKS_TAMEX4_FE1" (aka TwinPeaks v1) - and this is justified concerning its form factor, it's desired application, and its read-out approach. However, the analog electronics and their internal workings differ quite a bit and are not a mere refinement of TwinPeaks v1.

![](block_schema_all.svg)

**Twin_Peaks_CFD1** features:
- **16 analog signal inputs** (LEMO jacks), all $50\ \Omega$ terminated leading to
- 16 identical and independent **analog channels**, optimized for negative polarity detector signals from Scintillators coupled to Photomultiplier Tubes (**PMT**s)
- each analog input branches into two separate signal paths:
	- A **fast branch** featuring a Constant-Fraction Discriminator (**CFD**) for precisely extracting the time-of-arrival (**TOA**)
	- And a **slow branch** featuring an integrating charge measurement device we call **QFM** which creates a logic signal pulse of a specific length which is directly proportional to the detected charge (proportional to gamma ray energy)
- 16 channels with two branches each result in 32 logic output signals that are sent to the TAMEX4 TDC for digitization
- One **analog sum** output (LEMO jack) for implementing a fast analog multiplicity trigger for a detector array. All fast branch discriminator output signals are summed - each channel contributes with an amplitude of $100\  mV$. The analog sum outputs of multiple Twin_Peaks_CFD1 boards can be in turn summed, to achieve a multiplicity trigger for a detector with more than 16 channels.
- 3 configurable **NIM outputs** (LEMO jacks) that can repeat any (logic OR) combination of TDC input signals. A fourth NIM output is available at the cost of sacrificing the analog sum output (soldering required).
- An on-board **analog test pulser** (not to be confused with the digital TAMEX4 test pulser for TDC calibration). The analog test pulser serves to test both the fast and the slow branch of the analog signal processing channels. The test pulse for each channel can be enabled/disabled separately. When no cable is plugged into the Twin_Peaks_CFD1 input the test pulse has an amplitude of $35\ mV$ and contains a pulse charge of $33\  pC$, which corresponds to a "small but not super small" signal.


# CFD module

An open access paper has been published, describing the concept of the CFD circuit in detail:
https://iopscience.iop.org/article/10.1088/1748-0221/19/12/C12009/meta

![](block_schema_CFD.svg)
~~~
---------------------
TOO LONG DIDN'T READ:
---------------------
- set the ACC threshold wherever you want your low energy cut-off to be
- leave the ARM and ZC threshold at the default setting for the start
- the detector signals are negative, the preamps are inverting -> higher threshold settings (higher numbers) mean higher threshold
- after power on, the ACC threshold is set to the default value of 28000 (0x6d60)
- don't set thresholds higher than 50000 - nothing will break, but the threshold voltage will be out of the "sane" input voltage region of the comparators/discriminators 
~~~

- The CFD module comprises three signal branches, each with an individual fast amplifier and a discriminator (comparator) including a separate adjustable threshold.
- All amplifiers are inverting
- The constant fraction shaper works with a signal delay of 2.5 ns and a fraction setting of 30%.
- Ultimately all three branches are combined to create a single logic signal to be recorded by the TDC
- We distinguish between the ACC, ARM and ZC branches, each has its particular task:

**ACC branch:**
- The **ACC** discriminator is a simple leading-edge discriminator which processes the amplified raw signal. It has the widest threshold range.
- The **ACC** threshold determines which pulses get ACCepted and are consequently processed by the CFD as a whole. Set this threshold according to the highest X-ray energy that you want to exclude from your measurement (low energy cut-off).
- The **ACC** branch is subject to time-walk and is thus **not** used for timing.
- The **ACC** signal can get rather short, thus it is used to trigger a self-clearing latch with a configurable window length. The resulting **WIN** signal is used as a **gate** for the actual timing signal.

**ARM branch:**
- Just like the ACC discriminator, the **ARM** discriminator acts upon the amplified raw detector signal but its amplifier has a higher gain.
- The **ARM** branch is **not** used for timing.
- The threshold range is very limited. The threshold must be set just above the baseline noise of the detector to quickly react to incoming pulses with the least amount of time-walk.
- The **ARM** discriminator's task is to create an arming (enable) signal for the zero-crossing detector.

**ZC branch**
- This branch sees the the detector signal **after** the **constant fraction shaper** + amplifier.
- The **ZC** discriminator acts as a **zero-crossing detector** and extracts the time-of-arrival information of the detector signal without time walk (with greatly reduced walk)
- The **ZC** threshold needs to be empirically fine tuned to be exactly at baseline level for best CFD performance.
- It requires the ARM signal to be high, few ns before the zero crossing occurs.

The final output signal of the entire CFD is the delayed **ZC** signal gated with the **WIN** signal, i.e. it's leading edge carries the zero-crossing time, but the CFD output is only enabled when the detector signal surpassed the **ACC**ept threshold.



# QFM module

![](block_schema_QFM.svg)

Where physicists talk about measuring the **energy** (of a gamma), the analog front-end actually has to measure electrical **charge** (Q), because that's the physical quantity that the detector actually produces. But yes, this charge is proportional to the energy of the detected particle. The charge that we're after is the time integral of the current that flows out of the detector and into the analog input over the entire duration of the pulse (T).

$$ Q = \int_0^T\ I(t)\ dt $$

In this system we use a high precision TDC for digitization, so we need a device that measures the charge and converts it into a pulse width, if possible, with a linear transfer function.

**QFM** stands for charge (**Q**) measurement via frequency modulation (**FM**).

- The first stage is a low noise, low bandwidth amplifier that converts the input current signal (*actually* the voltage signal across the termination resistor *caused* by the input current) into a voltage signal with higher amplitude and a lower steepness while preserving the proportionality to the input charge.
- The second stage is a voltage controlled oscillator (**VCO**) that creates a square wave with a frequency depending on its control voltage (**CV**) input. The offset voltage of the CV input is constantly and automatically fine tuned so that the idle frequency of the VCO is $100\ MHz$ .
- The amplified detector signal is added to the CV input, modulating the frequency momentarily - exactly like an FM radio station. The higher the CV input - the slower the oscillator.
- The phase $\varphi$  of a periodic function is the time integral of its frequency $f$, analogously the charge $Q$ is the time integral of the current $I$ :

$$ \varphi(t) = \int_0^t f(t') dt' \ \Leftrightarrow \ Q(t) = \int_0^t I(t') dt' $$

- So if the input current directly changes the frequency ($f(t) \propto I(t)$), then measuring the phase yields the charge.
- Okay, but we can't measure the phase directly, we only get a rising edge, every time the VCO completes a full oscillation. But we can compare the time difference between a modulated VCO completing, let's say, 30 cycles relative to an undisturbed oscillator, completing 30 cycles at exactly $100\ MHz$. The time difference arises as a result of a cumulative phase shift over the counted cycles.
- Electronically, to measure the phase (relative to the idle oscillation) we employ a counter-like state machine (inside the on-board FPGA): When receiving a start signal, the state machine begins counting the rising edges at its CLK input. The output is set *HIGH* upon the first CLK edge and is cleared upon receiving the $n^{th}$ CLK edge. $n$ is programmable via the *qfm_cnt_cyc* register.
- Example: If $n=30$ and the start signal is triggered, QFM will produce a pulse that is $30\cdot\frac{1}{100\ MHz} = 300\ ns$ long if no detector signal ($Q=0$) is received.
- If a measurable amount of charge is produced by the detector and *start* was triggered *before* the detector pulse, then the resulting pulse length will be $300\ ns+x$ with $x\propto Q$.
- In practice, the *start* signal of the QFM counter is triggered with the earliest signal of the fast branch (the *WIN* signal). The combination of digital and analog delays (and some FPGA trickery) make sure that *start* appears to come before the actual current/charge signal.
- The number of counter cycles ($n=$ *qfm_cnt_cyc*) should be set, so the integration window of $n\cdot 10\ ns$ is long enough to contain an entire detector pulse to the point where the signal tail has sufficiently decayed. Envisioned values for $LaBr$ detectors are $n=30\ to\ 50$ $\Rightarrow T=300\ to\ 500\ ns$.
- Making the window shorter might increase your rate capability at the expense of charge precision. Making the window much longer than required leads to integrating more baseline noise and consequently to a degradation of the charge precision, as well as possible pile-up problems.

# Analog Test Pulser

- Twin_Peaks_CFD1 has an analog test pulser on-board which can couple a small pulse directly into the input pins of the analog channels.
- By default this pulser is off.
- Only a single amplitude (circa 35 mV) and thus a specific amount of charge is produced (33 pC).
- In order to measure the test pulse with its full amplitude, please unplug the input cable at the respective LEMO input jack.
- If you plug a 50R terminator into a LEMO input when the analog pulser is active, the test pulse amplitude is reduced to circa 17.5 mV (and a charge of 16.5 pC).
- If you set your thresholds low enough to see the analog test pulser, then you know you are close to the lowest pulse amplitudes the system can reilably detect.
- See the next section "Control Registers" for usage examples.



# Control Registers

- Twin_Peaks_CFD1 control registers are programmed by means of a tiny service python script:
~~~
# write argument order:
./pqdc1_spi.py <SFP> <device> <SPI chan> <register> <sub register> w <value>

# read back a value:
./pqdc1_spi.py <SFP> <device> <SPI chan> <register> <sub register> r 0

# example sets ACC threshold of ch 10 (lower FPGA, third ch) to 32000
./pqdc1_spi.py 1 0 1 0x00 2 w 32000

 
# enable test pulser (use only on FPGA 2 /spi channel 1)
./pqdc1_spi.py 1 0 1 0x03 3 w <16 bit mask for active channels>
# enable test pulser for channel 0 (first channel)
./pqdc1_spi.py 1 0 1 0x03 3 w 0x0001
# enable test pulser for channel 7+8 (8th and 9th channel)
./pqdc1_spi.py 1 0 1 0x03 3 w 0x0300
# test pulser off for all channels
./pqdc1_spi.py 1 0 1 0x03 3 w 0x0000

~~~

- SPI channel: chan 0 addresses upper FPGA, serving channels 1-8, chan 1 addresses lower FPGA, serving channels 9-16

| Register Name | **SPI channel** | Register Address | **Sub Register** | Type | Description |
| ---- | ---- | ---- | ---- | ---- | ---- |
| thr_ACC | 0/1 | 0x00 | 0-7 | 16 bit int | thresholds for **ACC**ept discriminator - default value = 28000<br>don't set higher than 50000 |
| thr_ARM | 0/1 | 0x01 | 0-7 | 16 bit int | thresholds for **ARM** discriminator - default value = 29000<br>don't set higher than 50000 |
| thr_ZC | 0/1 | 0x02 | 0-7 | 16 bit int | thresholds for **Z**ero-**C**rossing discriminator - default value = 28500,<br>don't set higher than 50000 |
| qfm_pre_dly | 0/1 | 0x03 | 0 | 3 bit int | delay line length between qfm oscillator and counter, 0 => shortest delay (default), 7 => longest delay |
| qfm_cnt_cyc | 0/1 | 0x03 | 1 | 6 bit int | QFM counter setting = number of $10\ ns$ cycles to count for a charge measurement, default = 50 |
| en_ana_TP | **!!! 1 only !!!** | 0x03 | 3 | 16 bit en/disable mask | enable analog test pulse for specific channel. this register selects all 16 channels, because the test pulser is controlled by the lower FPGA alone |
| en_ana_sum | 0/1 | 0x03 | 4 | 8 bit en/disable mask | select which channels contribute to analog sum |
| ana_sum_win_length | 0/1 | 0x03 | 5 | 8 bit int | window length for analog sum outputs expressed in multiples of $10\ ns$, default = 5 =>  $50\ ns$. If set to 0, the window function is disabled ana_sum out reflects the true TOT of the fast discriminator. |
| acc_win_length | 0/1 | 0x03 | 6 | 8 bit int | window length for the ACCept gate for the CFD expressed in multiples of $10\ ns$, default = 5 =>  $50\ ns$. If set to 0, the window function is disabled and the true TOT of the fast discriminator is used |
| fast_ch_mode | 0/1 | 0x03 | 7 | 4 bit int | 0 => LE discr.<br>1 => CFD |
# Front connector mapping

![](LEMO_mapping.png)


# FAQ
(or alternatively: fervently anticipated questions)

- Do we need to run our MBS crate with 19V - like with Twin_Peaks v1?
	- No. The new slow channel is based on a different technology and does not require special high voltages. Twin_Peaks_CFD1 should work with a mint MBS crate.
- Where is Twin Peaks 2?
	- Twin_Peaks_CFD1 *is* Twin Peaks 2. That is the name on the PCB now. Sorry for the confusion.
- Has Twin_Peaks_CFD1 the same physical dimensions as Twin_Peaks v1?
	- Mostly yes, but it is 15 mm longer, i.e. the LEMOs protrude farther out of the rack.
- Couldn't you have done the job of the overengineered QFM thing much easier with an ADC and some FPGA code?
	- Yes, but:
	1. You'd have a TDC *and* an ADC read-out runnig side by side.
	2. You'd *feel* the price of 16 high performance ADCs per board in your pockets
	3. You'd feel the heat of 16 high performance ADCs per board in your MBS rack
	4. You'd need a much more expensive FPGA on the Front-End board to do the number crunching
	5. QFM should enable you to measure a pulse every $500\ ns$ or so
- Does the slow channel need a threshold setting?
	- No. But you can set the number of oscillation cycles to be counted, i.e. the charge integration time window.
- Does the slow branch charge measurement calibration depend on the exact threshold(s) in the fast channel?
	- No. But of course, the ACC threshold also determines the low-energy cut-off in the charge/energy spectrum recorded by the slow channel.
 - How can we sum the analog multiplicity signals from two or more boards?
	 - I have a circuit simulation of a solution for that - We have to design and build another board when this feature is needed - but it will be a much simpler board than Twin_Peaks_CFD1. 
